import re
import json

def transaction(transaction_list):
    transactions = []
    lines = transaction_list.split('\n')
    lines = [re.sub('\s+', ' ', line.strip()) for line in lines if line.strip()]
    headers = lines.pop(0).split()
    print("Headers:", headers)

    for line in lines:
        transaction = {}
        columns = re.split(r'\s{2,}', line)
        print("Columns:", columns)  
        if len(columns) != len(headers):
            continue
        for i, header in enumerate(headers):
            transaction[header.lower()] = columns[i]
        transaction.append(transaction)
    
    transactions_json = json.dumps(transactions, indent=4)
    return transactions_json


#Test code 1
transaction_test1 = """Description            Date            Credit       Debit      Amount
                      Transaction 1       09/10/2023        10              -              110
                      Really Long        10/10/2023         -               55              55
                      Transaction 2"""
transactions_1 = transaction(transaction_test1)
print(transactions_1)

#Test code 2
transaction_test2 = """Description            Date            Credit       Debit      Amount
                      Transaction 2       10/10/2023        -              110              110
                      Really Long        10/10/2023         -               55              55
                      Transaction 4"""
transactions_2 = transaction(transaction_test2)
print(transactions_2)
